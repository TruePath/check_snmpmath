# check_snmpmath
Nagios style plugin that can perform math functions on SNMP results.

  (1) snmpget two OIDs and calculate a percentage. 
  This can be very useful when you have "mem total" and "mem used" OIDs and you want to display the percent used.

  (2) snmpget one OID and divide by a given number.

## Usage

    Usage: 

    check_snmpmath.sh [OPTIONS] -ou <OID> -ot <OID> 
    check_snmpmath.sh [OPTIONS] -ou <OID> -d <Integer>

    OPTIONS

      -h            Print this message
      -v  <1,2c,3>  SNMP version
      -c  <string>  SNMP community string
      -H  <host>    SNMP target host
      -W  <1-100>   Warn at x|%x (default 70)
      -C  <1-100>   Critical at x|%x (default 90)
      -d  <INT>     Divide OID_1 by this number
      -l  <label>   Metric label (default %Used)
      -ou <OID1>    OID_1 - Amount Used OR OID used when dividing by -d #
      -ot <OID2>    OID_2 - Amount Total

      --help same as -h
      --version Print version

## Example
./check_snmpmath.sh -v 2c -c public -H 192.168.2.6 -ou .1.3.6.1.4.1.22610.2.4.1.2.2.0 -ot .1.3.6.1.4.1.22610.2.4.1.2.1.0

OK - 37.16% |%Used=37.16%;70;90;0;100;

./check_snmpmath.sh -v 2c -c public -H 10.210.192.45:10000 -ou .1.3.6.1.4.1.22610.2.4.1.2.2.0 -d 10024 -C 20000 -W 10000

OK - 4825.76 |%Used=4825.76;10000;20000;0;100;


