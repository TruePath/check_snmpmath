#!/bin/bash
# ------------------------------------------------------------------
# Copywrite:  TruePath Technologies Inc.
# License:    Apache 2.0
# Author:     Patrick Byrne / Douglas Mauro
# Title:      check_snmpmath
# Description:
#       Do mathematical operations on SNMP results 
# ------------------------------------------------------------------
#    Todo:
# ------------------------------------------------------------------

version=1.4.0
snmpgetflags="-OvQU"
oid_list=()

# --- Functions ----------------------------------------------------

version()	
{
  printf "Version: %s\\n" $version
}

usage() 
{
    cat <<"STOP"
    Usage: 

    check_snmpmath.sh [OPTIONS] -ou <OID> -ot <OID> 
    check_snmpmath.sh [OPTIONS] -ou <OID> -d <Integer>

    OPTIONS

      -h            Print this message
      -v  <1,2c,3>  SNMP version
      -c  <string>  SNMP community string
      -H  <host>    SNMP target host
      -W  <1-100>   Warn at x|%x (default 70)
      -C  <1-100>   Critical at x|%x (default 90)
      -d  <INT>     Divide OID_1 by this number
      -l  <label>   Metric label (default %Used)
      -ou <OID1>    OID_1 - Amount Used OR OID used when dividing by -d #
      -ot <OID2>    OID_2 - Amount Total

      --help same as -h
      --version Print version

STOP
}

print_plugin_prct_out()
{
  # Build the plugin output string
  printf "%s - %s%% |%s=%s%%;%s;%s;0;100;\\n" \
    "$plugin_status" "$used_percent" "$metric_label" "$used_percent" \
      "$warn_trigger_value" "$crit_trigger_value"
}

print_plugin_math_out()
{
 printf "%s - %s |%s=%s;%s;%s;0;100;\\n" \
    "$plugin_status" "$calced_val" "$metric_label" "$calced_val" \
      "$warn_trigger_value" "$crit_trigger_value"
}


# --- Options processing -------------------------------------------

while [[ $# -gt 0 ]]; do
  param=$1
  value=$2
  case $param in
    -h | --help | help)
      usage
      exit
      ;;
    --version | version)
      version
      exit
      ;;
    -v)
      snmpversion="$value"
      shift
      ;;
    -c)
      snmpcommunity="$value"
      shift
      ;;
    -H)
      snmphost="$value"
      shift
      ;;
    -W)
      warn_trigger_value="$value"
      shift
      ;;
    -C)
      crit_trigger_value="$value"
      shift
      ;;
    -d)
      div_by="$value"
      shift
      ;;
    -l)
      metric_label="$value"
      shift
      ;;
    -ou)
      oid_list[0]="$value"
      shift
      ;;
    -ot)
      oid_list[1]="$value"
      shift
      ;;
    *)
      echo "Error: unknown parameter \"$param\""
      usage
      exit 3
      ;;
  esac
  shift
done

# --- Body ---------------------------------------------------------

# Check and set default the warning percent
[[ -z $warn_trigger_value ]] && warn_trigger_value=70

# Check and set default the critical percent
[[ -z $crit_trigger_value ]] && crit_trigger_value=90

# Check and set default the default label
[[ -z $metric_label ]] && metric_label="%Used"

# Use snmpget to pull both OIDs and mapfile to split the lines into an array
mapfile -t result <<< \
 "$(snmpget -v "$snmpversion" -c "$snmpcommunity" $snmpgetflags "$snmphost" "${oid_list[@]}")"

# If the divide by flag is set, divide the Used Oid
if [ -n "${div_by}" ]; then

  # Make sure Used OID and divide by are set
  if [[ -z ${oid_list[0]} || -z $div_by ]]; then
    echo "Error: Used OID and/or divide by, not set"
    usage
    exit 1
  fi
	
  # Verify that we get numeric results
  if [[ ! ${result[0]} -gt 0 ]] || [[ ! ${div_by} -gt 0 ]] || \
    [[ ! ${result[0]} =~ ^[0-9]+$ ]] || [[ ! ${div_by} =~ ^[0-9]+$ ]]; then
    echo "Error: Unexpected result from host"
    exit 3
  fi

  # Find the percent used and round it properly
  calced_val="$(awk "BEGIN {printf \"%.2f\\n\", (${result[0]}/${div_by})}")"

  # Check percent used status
  if [[ $(echo "$calced_val < $warn_trigger_value" | bc -l) = 1 ]]; then
    plugin_status="OK"
    print_plugin_math_out
    exit 0
  elif [[ $(echo "$calced_val < $crit_trigger_value" | bc -l) = 1 ]]; then
    plugin_status="WARN"
    print_plugin_math_out
    exit 1
  else
    plugin_status="CRIT"
    print_plugin_math_out
    exit 2
  fi

else
  # Make sure Used and Total OIDs are set 
  if [[ -z ${oid_list[0]} || -z ${oid_list[1]} ]]; then
    echo "Error: Used and Total OIDs are not set"
    usage
    exit 1
  fi

	# Check to make sure we got two numeric values greater than 0 from our OIDs
	if [[ ! ${result[0]} -gt 0 ]] || [[ ! ${result[1]} -gt 0 ]] || \
	[[ ! ${result[0]} =~ ^[0-9]+$ ]] || [[ ! ${result[1]} =~ ^[0-9]+$ ]]; then
		echo "Error: Unexpected result from host"
    usage
		exit 3
	fi

	# Find the percent used and round it properly
	used_percent="$(awk "BEGIN {printf \"%.2f\\n\", (${result[0]}/${result[1]})*100}")"

	# Check percent used status
	if [[ $(echo "$used_percent < $warn_trigger_value" | bc -l) = 1 ]]; then
		plugin_status="OK"
		print_plugin_prct_out
		exit 0
	elif [[ $(echo "$used_percent < $crit_trigger_value" | bc -l) = 1 ]]; then
		plugin_status="WARN"
		print_plugin_prct_out
		exit 1
	else 
		plugin_status="CRIT"
		print_plugin_prct_out
		exit 2
	fi
fi
